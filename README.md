# Test univention

## Introduction

A Python script should be implemented which moves all files from all members of a group to an archive folder. The name of the group should be a parameter of the program.
The program should be robust, e.g. with respect to multiple invocations in a short time. The events and results should be available in a log file.
The program should be installable as a Debian package and should be made available for download. The Debian package does not have to be built, the sources are sufficient.
