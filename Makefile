ENV_NAME=univention-test
REPO_PATH="Xemyst/test-univention"
REPO_FULL_PATH="registry.gitlab.com/${REPO_PATH}"

configure:
	virtualenv ${ENV_NAME}
	source ${ENV_NAME}/bin/activate
	pip3 install -r requirements.txt

activate:
	source ${ENV_NAME}/bin/activate

deactivate:
	deactivate

build:
	
